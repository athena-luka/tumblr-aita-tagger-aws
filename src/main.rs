use lambda_runtime::{run, service_fn, Error, LambdaEvent};
use serde::{Serialize, Deserialize};

use tumblr_aita_tagger::TumblrHandler;

#[derive(Deserialize)]
struct Request {}

#[derive(Serialize)]
struct Response {}

async fn function_handler(_event: LambdaEvent<Request>) -> Result<Response, Error> {
    let mut tumblr_handler = TumblrHandler::new().await;
    tumblr_handler.auto_tag_posts().await?;

    Ok(Response {})
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    run(service_fn(function_handler)).await
}
