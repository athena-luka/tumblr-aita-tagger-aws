This is a simple wrapper around [tumblr-aita-wrapper](https://www.gitlab.com/athena-luka/tumblr-aita-wrapper),
to make it work as an AWS Lambda function.

## License

Copyright &copy; 2023 Athena Luka.

This project is licensed under either of

- [Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0) ([`LICENSE-APACHE`](LICENSE-APACHE))
- [MIT License](https://opensource.org/licenses/MIT) ([`LICENSE-MIT`](LICENSE-MIT))

at your option.

The [SPDX](https://spdx.dev) license identifier for this project is `MIT OR Apache-2.0`.
